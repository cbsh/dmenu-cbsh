static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#51afef", "#3f444a" },
	[SchemeSel] = { "#21242b", "#98be65" },
	[SchemeOut] = { "#000000", "#2257A0" },
	[SchemeMid] = { "#da8548", "#21242b" },
};
