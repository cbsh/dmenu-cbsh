static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#4790cd", "#285274" },
	[SchemeSel] = { "#3f3049", "#f0ee79" },
	[SchemeOut] = { "#000000", "#4cb1ff" },
	[SchemeMid] = { "#4790cd", "#3f3049" },
};
