static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#f5c1ce", "#94536d" },
	[SchemeSel] = { "#351e28", "#ed85ae" },
	[SchemeOut] = { "#000000", "#4cb1ff" },
	[SchemeMid] = { "#f5c1ce", "#351e28" },
};
