static const char *colors[SchemeLast][2] = {
	/*                   fg         bg       */
	[SchemeNorm] = { "#626C6C", "#002b36" },
	[SchemeSel] = { "#BBBBBB", "#204052" },
	[SchemeOut] = { "#000000", "#2aa198" },
	[SchemeMid] = { "#626C6C", "#073642" },
};
