static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#77345e", "#8d173e" },
	[SchemeSel] = { "#26111e", "#bc1e52" },
	[SchemeOut] = { "#000000", "#77c7d4" },
	[SchemeMid] = { "#77345e", "#26111e" },
};
