static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#93112d", "#c33d10" },
	[SchemeSel] = { "#280e08", "#17ad84" },
	[SchemeOut] = { "#000000", "#ee6e23" },
	[SchemeMid] = { "#93112d", "#280e08" },
};
