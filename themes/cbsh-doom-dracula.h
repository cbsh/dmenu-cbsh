static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bd93f9", "#44475a" },
	[SchemeSel] = { "#1E2029", "#f1fa8c" },
	[SchemeOut] = { "#000000", "#50fa7b" },
	[SchemeMid] = { "#ff79c6", "#565761" },
};
