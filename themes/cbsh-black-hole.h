static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#2EA3EE", "#0A1533" },
	[SchemeSel] = { "#7CEEF6", "#165099" },
	[SchemeOut] = { "#000000", "#7CEEF6" },
	[SchemeMid] = { "#2EA3EE", "#0D1F73" },
};
