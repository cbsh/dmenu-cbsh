static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#4f77a2", "#8a3340" },
	[SchemeSel] = { "#23345f", "#d65064" },
	[SchemeOut] = { "#000000", "#9fd2f1" },
	[SchemeMid] = { "#4f77a2", "#23345f" },
};
