static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#e1acff", "#723C87" },
	[SchemeSel] = { "#241B30", "#EB66D5" },
	[SchemeOut] = { "#000000", "#00ffff" },
	[SchemeMid] = { "#e1acff", "#241B30" },
};
