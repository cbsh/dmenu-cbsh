static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#b1a5d5", "#68617d" },
	[SchemeSel] = { "#3a1e28", "#f27ea3" },
	[SchemeOut] = { "#000000", "#9fd2f1" },
	[SchemeMid] = { "#b1a5d5", "#3a1e28" },
};
